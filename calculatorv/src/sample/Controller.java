package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller  implements Initializable{
    @FXML
    private TextField pole;

    public Button btn_one;
    public Button btn_two;
    public Button btn_three;
    public Button btn_four;
    public Button btn_five;
    public Button btn_six;
    public Button btn_seven;
    public Button btn_eight;
    public Button btn_nine;
    public Button btn_zero;
    public Button divide;
    public Button plus;
    public Button minus;
    public Button multiply;
    public Button result;
    public Button clear;
    public Button backspace;
    public Button dot;

    private double fnumber;
    private double snumber;
    private String operator;
    public Label show;


    @FXML
    public void dodaj(ActionEvent event) {

        //System.out.print("dodawanie!");
        String value= pole.getText();
        double valuenumber = Double.parseDouble(value);
        this.fnumber=valuenumber;
        pole.setText("");
        show.setText(value + "+");
        operator = "+";
    }
    public void odejmowanie(ActionEvent event) {

        String value= pole.getText();
        double valuenumber = Double.parseDouble(value);
        this.fnumber=valuenumber;
        pole.setText("");
        show.setText(value + "-");
        operator = "-";

    }
    public void dzielenie(ActionEvent event) {
        String value= pole.getText();
        double valuenumber = Double.parseDouble(value);
        this.fnumber=valuenumber;
        pole.setText("");
        show.setText(value + "/");
        operator = "/";


    }
    public void mnozenie() {
        String value= pole.getText();
        double valuenumber = Double.parseDouble(value);
        this.fnumber=valuenumber;
        pole.setText("");
        show.setText(value + "*");
        operator = "*";



    }
    public void wynik(ActionEvent event) {
        switch(operator) {
            case "+":
                String v = pole.getText();
                this.snumber = Double.parseDouble(v);
                pole.setText(String.valueOf(dodawanie()));
                String before = show.getText();
                show.setText(before + v);
                break;
            case "-":
                String vminus = pole.getText();
                this.snumber = Double.parseDouble(vminus);
                double sminus = this.fnumber - this.snumber;
                pole.setText(String.valueOf(sminus));
                String beforeminus = show.getText();
                show.setText(beforeminus + vminus);

                break;
            case "*":
                String vm = pole.getText();
                this.snumber = Double.parseDouble(vm);
                double sm = this.fnumber * this.snumber;
                pole.setText(String.valueOf(sm));
                String beforem = show.getText();
                show.setText(beforem + vm);

                break;
            case "/":
                String vdiv = pole.getText();
                this.snumber = Double.parseDouble(vdiv);
                double sdiv = this.fnumber / this.snumber;
                if (this.snumber != 0)
                {
                    pole.setText(String.valueOf(sdiv));
                    String beforediv = show.getText();
                    show.setText(beforediv + vdiv);

                } else if (this.snumber == 0)
                {

                    Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                    errorAlert.setHeaderText("Niepoprawna operacja");
                    errorAlert.setContentText("Nie można dzielić przez 0!");
                    errorAlert.showAndWait();

                }
                break;

        }
    }

    private double dodawanie() {
        double c =this.snumber +this.fnumber;
        return c;
    }


    public void clearClick(ActionEvent event) {
        pole.setText("");
        show.setText("");
        this.snumber=0;
        this.fnumber=0;
    }

    public void backspaceClick(ActionEvent event){
        String backspaceC;
        if(pole.getText().length() >0){
            StringBuilder B = new StringBuilder(pole.getText());
            B.deleteCharAt(pole.getText().length()-1);
            backspaceC = B.toString();
            pole.setText(backspaceC);
        }
    }

    public void dotClick(ActionEvent event){
        String wartosc=pole.getText();
        String ustaw= ".";
        pole.setText(wartosc + ustaw);

    }
    public void oneClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "1";
        pole.setText(wartosc + ustaw);
    }
    public void twoClick(ActionEvent event) {
        String wartosc=pole.getText();
        String ustaw= "2";
        pole.setText(wartosc + ustaw);
    }
    public void threeClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "3";
        pole.setText(wartosc + ustaw);
    }
    public void fourClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "4";
        pole.setText(wartosc + ustaw);
    }
    public void fiveClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "5";
        pole.setText(wartosc + ustaw);
    }
    public void sixClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "6";
        pole.setText(wartosc + ustaw);
    }
    public void sevenClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "7";
        pole.setText(wartosc + ustaw);
    }
    public void eightClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "8";
        pole.setText(wartosc + ustaw);
    }
    public void nineClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "9";
        pole.setText(wartosc + ustaw);
    }
    public void zeroClick(ActionEvent event) {

        String wartosc=pole.getText();
        String ustaw= "0";
        pole.setText(wartosc + ustaw);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
